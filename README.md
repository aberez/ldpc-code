Loopy Belief Propagation algorithm for low-density parity-check (LDPC) code.
For more information about this problem see "/report/problem definition.pdf".
