%% runJ. ���� �� �������� ��������� �������� ��� ��������� �������� J
disp('runJ start');
ERRfromJ=[];
q1=0.1 
N=80; 
K=20;

for j=3:9
    H=make_ldpc_mex(N-K, N, j);
    [G, ind] = ldpc_gen_matrix(H);
 if (sum(sum(mod(H*G,2)))~=0)
    disp('test H*G=0 failed');
 end
[err_bit, err_block, diver] = ldpc_mc(H, G, q1, 100);
ERRfromJ=[ERRfromJ; j err_bit err_block diver];
end
ERRfromJ
createfigureR(ERRfromJ(:,1),ERRfromJ(:,[4 2 3]));