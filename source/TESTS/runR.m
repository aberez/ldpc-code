%% runR. ���� �� �������� ��������� �������� ��� ������������ R
disp('runR start');
ERRfromR=[];
%R=0.6
N=40
q1=0.2
j=3

for R=0.1:0.04:0.5
    %N
    K=round(R*N);
    H=make_ldpc_mex(N-K, N, j);
    [G, ind] = ldpc_gen_matrix(H);
 if (sum(sum(mod(H*G,2)))~=0)
    disp('test H*G=0 failed');
 end
[err_bit, err_block, diver] = ldpc_mc(H, G, q1, 100);
ERRfromR=[ERRfromR; R err_bit err_block diver];
end
ERRfromR
createfigureR(ERRfromR(:,1),ERRfromR(:,[4 2 3]));