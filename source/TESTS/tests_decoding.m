%���� ��� �������� ������������ ������ ldpc_decoding
disp('tests_decoding_start'); 
N=12
    K=4
    q=0.1
for test=1:20

H=make_ldpc_mex(N-K, N, 3);
%H=make_ldpc_mex(round(0.75*50), 50, 3);
[G, ind] = ldpc_gen_matrix(H);

if (sum(sum(mod(H*G,2)))~=0)
   disp('test H*G=0 failed');  
end
t=round(rand(size(G,2),1));
x=mod(G*t,2);

%y=x;
%noise=unique(round(  rand( mod(2+test,6) ,1)*size(x,1) ))
%y(noise)=1-y(noise);

    noise=rand(size(x))<q;
    y=mod(x+noise,2);
    e_true=mod(y+x,2);

z=mod(H*y,2);
[e, status] = ldpc_decoding(z, H, q);
if status==0
    if all(e==e_true)
        disp('test accepted');
    else
        disp('test faied');
        [e e_true]
    end
else 
    disp('bad status');
    [e, status] = ldpc_decoding(z, H, q);
end 
end