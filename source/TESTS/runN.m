%% runN. ���� �� �������� ��������� �������� ��� ��������� �������� N
disp('runN start');
ERRfromN=[];
R=0.6
q1=0.05
j=3

for N=100:50:700
    %N
    K=round(R*N);
    H=make_ldpc_mex(N-K, N, j);
    [G, ind] = ldpc_gen_matrix(H);
 if (sum(sum(mod(H*G,2)))~=0)
    disp('test H*G=0 failed');
 end
[err_bit, err_block, diver] = ldpc_mc(H, G, q1, 100);
ERRfromN=[ERRfromN; N err_bit err_block diver];
end
ERRfromN
createfigureR(ERRfromN(:,1),ERRfromN(:,[4 2 3]));