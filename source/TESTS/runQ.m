%% runQ. ���� �� �������� ��������� �������� ��� ��������� �������� q
disp('runJ start');
ERRfromQ=[];
j=2 
N=20 
K=10
%3 16 4
%3 24 10

for q=0.01:0.01:0.20
    H=make_ldpc_mex(N-K, N, j);
    [G, ind] = ldpc_gen_matrix(H);
 if (sum(sum(mod(H*G,2)))~=0)
    disp('test H*G=0 failed');
 end
[err_bit, err_block, diver] = ldpc_mc(H, G, q, 100);
ERRfromQ=[ERRfromQ; q err_bit err_block diver];
end
ERRfromQ
createfigureR(ERRfromQ(:,1),ERRfromQ(:,[4 2 3]));