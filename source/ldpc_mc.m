%[err_bit, err_block, diver] = ldpc_mc(H, G, q, num_points)
%����
%H � ����������� ������� ��������, �������� ������� ������� MxN;
%G � ����������� ������� ����, �������� ������� ������� Nx(N-M);
%q � ����������� �������� ���� ��� �������� �� ������ �����, ����� �� 0 �� 0.5;
%num_points � ����� ���������� �������������, �����;
%�����
%err_bit � ����������� ������� ������ ������������� (������������ N ��� �������� �����), ����� �� 0 �� 1;
%err_block � ����������� �������� ������ �������������, ����� �� 0 �� 1;
%diver � ���� �������� ������������ ��������� �������������, ����� �� 0 �� 1.

function [err_bit, err_block, diver] = ldpc_mc(H, G, q, num_points)

display=false;% �������� �� �������������� ����������
err_bit=0;
err_block=0;
diver=0;
for numTest=1:num_points
    %������� ��������� ������
    t=round(rand(size(G,2),1));
    %�������� ���
    x=mod(G*t,2);
    N=size(x,1);
    %��������� ��� ������
    noise=rand(size(x))<q;
    y=mod(x+noise,2);
    
    %���������� ��������� � ��������� (� ��������� ����� == ���)
    e_true=mod(y+x,2);
    
    %������� �������
    z=mod(H*y,2);
    %��������� ��������
    [e, status] = ldpc_decoding(z, H, q);
    if status~=0
        if display
            disp('error: test faied with status -1');
            t'
            x'
            y'
            noise'
            [e_true e]
        end
        diver=diver+1;
        %err_bit=err_bit+any(e~=e_true);
        %err_block=err_block+sum(e~=e_true)/N;
        continue
    else
        if all(e==e_true)
            if display
            disp('test accepted');
            end
        else
             if display
            disp('test faied');
            end
            err_bit=err_bit+any(e~=e_true);
            err_block=err_block+sum(e~=e_true)/N;
        end
    end
 
end
err_bit=err_bit/num_points;
err_block=err_block/num_points;
diver=diver/num_points;
end

