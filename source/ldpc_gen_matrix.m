%[G, ind] = ldpc_gen_matrix(H)
% ����
%  H � ����������� ������� ��������, �������� ������� ������� MxN;
%
% �����
%  G � ����������� ������� ����, �������� ������� ������� Nx(N-M);
%  ind � ������ ������� �������� �����, � ������� ���������� ���� 
%  ��������� ���������, �.�. G(ind, :) �������� ��������� ��������.
function [G, ind] = ldpc_gen_matrix(H)
%H=full(H); %or you can use just sparse matrix
indcol=[];
rows=0;
[m n]=size(H);
for i=1:n %�� ���� ��������
    [index,~,~]=find(H(:,i)); %������-������� = ������ ����� � 1 ����������
    pElemSet=setdiff(index,1:rows); %��������� ������� ���������
    if (isempty(pElemSet))
        continue;
    end
    pElem=pElemSet(1); %������� �������
    rows=rows+1;
    %������ ������ �������
    tmp=H(pElem,:);
    H(pElem,:)=H(rows,:);
    H(rows,:)=tmp;
    indcol=[indcol i]; %����� ��������������� �������

    rowsFsum=setdiff(find(H(:,i)),rows);
    H(rowsFsum,:)=mod( H(rowsFsum,:) + repmat(H(rows,:),size(rowsFsum,1),1),2);
end
MatP=H(:,setdiff(1:n,indcol));%indrow
G=zeros(n,n-m);
G(indcol,:)=MatP;
G(setdiff(1:n,indcol),:)=eye(n-m);
ind=setdiff(1:n,indcol);
end