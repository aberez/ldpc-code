%[e, status] = ldpc_decoding(z, H, q, param_name1, param_value1, ...)
% ����
% z � ����������� �������, �������� ������-������� ����� M;
% H � ����������� ������� ��������, �������� ������� ������� MxN;
% q � ����������� �������� ���� ��� �������� �� ������ �����, ����� �� 0 �� 0.5;
% (param_name, param_value) � ����� �������������� ���������� ���������, ��������� ����� � �������� ��������:
% 'max_iter' � ������������ ����� �������� ��������� �������������, �����, �� ��������� = 200;
% 'eps' � ����� ������������ ��� ���������, �����, �� ��������� = 1e-4;
% 'display' � ����� �����������, true ��� false, ���� true, �� ������������ ������������� ���������� �� ���������, ��������, ����� ��������, ������� ����� ������ �������������, ������� ��� ��������� � �.�.
% �����
% e � ��������������� ������ ������, �������� ������-������� ����� N;
% status � ��������� �������������, ����� 0, ���� ������ e ������������ ��� ������, ����� -1, ���� ��������� ����� �� ������������� ����� �������� ��� ������������ �������� ���������.
function [e, status] = ldpc_decoding(z, H, q, varargin)

%% ��� 0. ������ ���������� (parse input)
p=inputParser;
default_max_iter=200;
default_eps=1e-4;
default_display=false;

addParamValue(p,'max_iter',default_max_iter);
addParamValue(p,'eps',default_eps);
addParamValue(p,'display',default_display,@islogical);

parse(p,varargin{:});

max_iter=p.Results.max_iter;
eps=p.Results.eps;
display=p.Results.display;
%end. parse input

%% ��� 1. ������������� 
iter=1;
if display
    iter
end
probab0_old=zeros(1,size(H,2));
H=full(H); %or you can use just sparse matrix
matXF0=H*(1-q); 
matXF1=H*q;

%% ��� ����. ������ �����
while (true)

%% ��� 2. �������� ��������� �� ��������
deltaXF=matXF0-matXF1;
%������������ �� ���� ������ (�� ���� ����������, �������� � ������ f_m) (��� n' in N(m)
allFX=prod(deltaXF.^H,2).*((-1).^(z));
% allFX(allFX<1e-5)=0;
%��������� ��� ������� m � n delta_{x_n->f_m}

 matFX=bsxfun(@times,allFX,H)./(deltaXF.^H);

 matFX(isnan(matFX))=0;
 
matFX0=(H+matFX)/2;
matFX1=(H-matFX)/2;

%% ��� 3. �������� ��������� �� ����������
%������������ �� ���� �������� (�� ���� ��������, ������������ ����������
%x_n) (��� m' in N(n) )
allXF0=prod(matFX0.^H,1);
allXF1=prod(matFX1.^H,1);
%������, ���� �����-�� ������� matFX0 ��� matFX1 ����� ����, �� ����� �����
%��� ������������ � �������, �.�. allFX ��� �������, � ������ ������ ��
%����� ����� 0, � 0/0 ������ (�� ���� ����� ������ 0 �� 1)
 matFX0(matFX0<1e-5)=1;
 matFX1(matFX1<1e-5)=1;
%��������� ��� ������� m � n matFX_{f_m->x_n} � ��������� �� �����������
%0 ~ ���������� �� ��������� 1 ~ ���� ���������� 
matXF0=(1-q) * bsxfun(@times,allXF0,H)./(matFX0.^H);
matXF1=q     * bsxfun(@times,allXF1,H)./(matFX1.^H);

matXF0(isnan(matXF0))=0;
matXF1(isnan(matXF1))=0;

probab0=(1-q)*allXF0;
probab1=q*    allXF1;
%������������
sumXF=matXF0+matXF1;
matXF0=matXF0./(sumXF.^H);
matXF1=matXF1./(sumXF.^H);

sumprobab=probab0+probab1;
probab0=probab0./sumprobab;
probab1=probab1./sumprobab;


%% ��� 4. ������ ������� ������ e
elook=(probab1>probab0);
%������ �� elook ������-�������
 if (size(elook,2)>1) 
     elook=elook';
 end
 if (display)  
     elook  
 end

%%��� 5. �������� ��������
 if (mod(H*elook,2)==z)
   status=0;
   e=elook;
   if (display)  
     disp('e is found'); 
   end
   return
 end
 if (iter>max_iter)
     status=-1;
     e=elook;
      if (display)  
       disp('exceeded max_num of iterations'); 
      end
     return
 end
 if (sum(abs(probab0_old-probab0))<eps && iter>2)
     status=-1;
     e=elook;
     if (display)  
       disp('metric is too small (mean(abs(e_old-elook))<eps)'); 
     end
     return;
 end
 iter=iter+1;
 if (display)  
        
 end
 probab0_old=probab0;
end

end